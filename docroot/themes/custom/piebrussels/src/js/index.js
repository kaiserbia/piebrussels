import $ from 'jquery'
import AOS from 'aos'
import Scroll from './scroll.js'
import Overflow from './overflow.js'

$(document).ready($ => {
  Scroll.init()
  Overflow.init()

  AOS.init({
    // Global settings:
    offset: 20, // offset (in px) from the original trigger point
    delay: 0, // values from 0 to 3000, with step 50ms
    duration: 1000, // values from 0 to 3000, with step 50ms
    once: false, // whether animation should happen only once - while scrolling down
    mirror: false, // whether elements should animate out while scrolling past them
    anchorPlacement: 'top-bottom', // defines which position of the element regarding to window should trigger the animation
  });
})
