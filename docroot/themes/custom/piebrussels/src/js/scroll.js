import $ from 'jquery'

class Scroll {
    constructor() {
    }

    init() {

      $('.block-block-content h2').attr("data-aos","fade-up")

      $('.field__item').attr("data-aos","fade-up")

      $('.block-webform-block, #webform_submission_newsletter_block_content_13_add_form-ajax').attr("data-aos","fade-left")

      $('.field--name-field-icon-list-reference').children('.field__item').attr("data-aos","zoom-in")

      $('.cards--list .views-view-grid .views-col').attr("data-aos","zoom-in-up")

      $('.paragraph--type--image-list .field--name-field-item-image').attr("data-aos","flip-left")

      $( '.field--name-field-hero-image .media__image' ).addClass('zoom')
    }
}

export default new Scroll
