const gulp        = require('gulp')
const sass        = require('gulp-sass')
const notify      = require('gulp-notify')
const sassGlob    = require('gulp-sass-glob')
const babel       = require('gulp-babel')
const plumber     = require('gulp-plumber')
const uglify      = require('gulp-uglify')
const rename      = require('gulp-rename')
const concat      = require('gulp-concat')
const merge       = require('gulp-merge')
const strip       = require('gulp-strip-comments')
const sourcemaps  = require('gulp-sourcemaps')

const browserify = require('browserify')
const babelify   = require('babelify')
const source     = require('vinyl-source-stream')
const buffer     = require('vinyl-buffer')

const BABEL_POLYFILL = './node_modules/babel-polyfill/browser.js'
const JS_FILES = 'src/js/index.js'

const CLIENT_BABEL_OPTS = {
  plugins: ['transform-flow-strip-types']
}

const PLUMBER_ERROR_HANDLER = {
  errorHandler: notify.onError({
    title: 'Gulp',
    message: "Error: <%= error.message %>"
  })
}

gulp.task('sass', () => (
  gulp.src('src/scss/main.scss')
  .pipe(sourcemaps.init())
  .pipe(plumber(PLUMBER_ERROR_HANDLER))
  .pipe(sassGlob())
  .pipe(sass({outputStyle: 'compressed'})
  .on('error', sass.logError))
  .pipe(rename({ suffix: '.min' }))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest('dist/css'))
))

// gulp.task('scripts', () =>
//   merge(
//     gulp.src(BABEL_POLYFILL),
//     gulp.src(JS_FILES)
//       .pipe(sourcemaps.init())
//       .pipe(plumber(PLUMBER_ERROR_HANDLER))
//       .pipe(babel(CLIENT_BABEL_OPTS))
//       .pipe(sourcemaps.write())
//   )
//     .pipe(sourcemaps.init({loadMaps: true}))
//     .pipe(strip())
//     .pipe(concat('script.js'))
//     .pipe(rename({ suffix: '.min' }))
//     .pipe(sourcemaps.write('.'))
//     .pipe(gulp.dest('dist/js'))
// )

gulp.task('scripts', () => {
  return browserify('src/js/index.js')
      .transform('babelify', {presets: ['env']})
      .bundle()
      .pipe(source('script.js'))
      .pipe(rename({ suffix: '.min' }))
      .pipe(buffer())
      .pipe(gulp.dest('dist/js'))
})

gulp.task('default', () => {
  gulp.watch(['src/scss/*.scss', 'src/scss/**/*.scss'], ['sass'])
  gulp.watch(['src/js/*.js', 'src/js/**/*.js'], ['scripts'])
})
